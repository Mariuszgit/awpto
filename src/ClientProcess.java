import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class ClientProcess extends Thread {

    public String name;
    public Socket socket;
    public BufferedReader bufferedReader;

    public ClientProcess(Socket socket, String name) {
        this.socket = socket;
        this.name = name;
    }

    @Override
    public void run() {

        try {
            bufferedReader = new BufferedReader( new InputStreamReader(socket.getInputStream()) );

            while(true) {
                String message = bufferedReader.readLine();
                System.out.println(message);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bufferedReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }
}