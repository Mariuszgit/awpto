import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class ChatClient {

    public static final int SERVER_PORT = 2137;
    public static final String SERVER_IP = "127.0.0.1";
    static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        String name = "";

        while(true) {
          name = register();
            if(name.isEmpty()) {
                System.out.println("Twoj nick nie moze byc pusty :( .");
                continue;
            }
           break;
        }

        Socket socket = new Socket();
        try {
            socket.connect( new InetSocketAddress(SERVER_IP, SERVER_PORT) );
            System.out.println("Połączono z serwerem: "+SERVER_IP+":"+SERVER_PORT);


            ClientProcess recieveThread = new ClientProcess(socket, name);
            recieveThread.start();
            PrintWriter printWriter = new PrintWriter( new OutputStreamWriter(socket.getOutputStream())  );
            String request = "j:" + name;
            printWriter.println(request);
            printWriter.flush();

            while(true) {
                String message = scanner.nextLine();

                if(message.isEmpty()) {
                    System.out.println("Twoja wiadomosc nie moze byc pusta");
                    continue;
                }

                if("zakoncz".equals(message)) {
                    System.exit(666);
                } else {
                    message = "m:" + message;
                }
                printWriter.println(message);
                printWriter.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                scanner.close();
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static String register(){
        System.out.println("Podaj nazwe uzytkownika.");
        System.out.print("Nick -> ");
       return  scanner.nextLine();
    }

}