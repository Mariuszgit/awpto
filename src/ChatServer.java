import java.io.*;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServer {
    public static final int PORT = 2137;
    public static Integer MAX_THREADS = 4;


    public static void main(String[] args) {

        ServerSocket serverSocket = null;
        List<PrintWriter> listSocketWriters = new ArrayList<>();

        ExecutorService executorService = null;

        try {
            String hostAddr = "127.0.0.1";
            executorService = Executors.newFixedThreadPool(MAX_THREADS);
            serverSocket = new ServerSocket();
            serverSocket.bind( new InetSocketAddress(hostAddr, PORT) );

            while(true) {
                Socket socket = serverSocket.accept();
                ServerProcess thread = new ServerProcess(socket, listSocketWriters);

                executorService.execute(thread);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            executorService.shutdown();
            try {
                if( serverSocket != null && !serverSocket.isClosed())
                    serverSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}