import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ServerProcess implements Runnable{

    public String nickname = null;
    public Socket socket = null;
    public List<PrintWriter> listSocketWirter = null;
    FileOutputStream logFileStream = null;

    public ServerProcess(Socket socket, List<PrintWriter> listSocketWirter) {
        this.socket = socket;
        this.listSocketWirter = listSocketWirter;
    }

    @Override
    public void run() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        String currentTime = dateFormat.format(new Date());

        try {
            BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(socket.getInputStream()) );
            PrintWriter printWriter = new PrintWriter( new OutputStreamWriter(socket.getOutputStream()));
            logFileStream = new FileOutputStream("chatlog.txt", true);

            while(true) {
                String request = bufferedReader.readLine();

                String[] tokens = request.split(":");
                if("j".equals(tokens[0])) {
                    join(tokens[1], printWriter);
                    fileWrite(" dołączył.", currentTime, logFileStream);
                } else if("m".equals(tokens[0])) {
                    sendMessage(tokens[1]);
                    fileWrite( " : " + tokens[1], currentTime, logFileStream);
                } else if("q".equals(tokens[0])) {
                    quit(printWriter, currentTime);
                    fileWrite(" opuścił czat.", currentTime, logFileStream);
                }
            }
        } catch (IOException e) {
           System.out.println(this.nickname + " odłączył się od serwera.");
        } finally {
            fileWrite(" odłączył się od serwera.", currentTime, logFileStream);
            closeFile(logFileStream);
        }
    }

    public void quit(PrintWriter writer, String curDate) {
        removeWriter(writer);

        String data = "[" + curDate + "] " + this.nickname + " odłączył się od serwera.";
        broadCast(data);
    }

    public void removeWriter(PrintWriter writer) {
        synchronized (listSocketWirter) {
            listSocketWirter.remove(writer);
        }
    }

    public void sendMessage(String data) {
        String message;
        if (data.isEmpty())
            message = "";
        else
            message = this.nickname + " : " + data;

        broadCast(message);
    }

    public void join(String nickname, PrintWriter writer) {
        this.nickname = nickname;

        String data = nickname + " dołączył.";
        broadCast(data);

        addWriter(writer);
    }

    public void addWriter(PrintWriter writer) {
        synchronized (listSocketWirter) {
            listSocketWirter.add(writer);
        }
    }

    public void broadCast(String data) {
        synchronized (listSocketWirter) {
            for(PrintWriter writer : listSocketWirter) {
                writer.println(data);
                writer.flush();
            }
        }
    }

    public void fileWrite(String data, String curDate, FileOutputStream fileOutputStream) {
        String message = "[" + curDate + "] " + this.nickname + data +"\n";
        synchronized (fileOutputStream){
            try{
                fileOutputStream.write(message.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void closeFile(FileOutputStream fileOutputStream) {
        synchronized (fileOutputStream) {
            try {
                fileOutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}